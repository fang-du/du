package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.SysImg;
import com.ruoyi.system.mapper.SysImgMapper;
import com.ruoyi.system.service.ISysImgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysImgServiceImpl implements ISysImgService {
    @Autowired
    private SysImgMapper imgMapper;

    @Override
    public int inserConstructionId(Long constructionId) {
        return imgMapper.inserConstructionId(constructionId);
    }

    @Override
    public int insertImg(SysImg img) {
        return imgMapper.insertImg(img);
    }
}
