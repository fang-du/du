package com.ruoyi.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.common.core.domain.entity.SysPersonnel;
import com.ruoyi.common.core.domain.entity.SysRole;

import java.util.List;

public interface ISysPersonnelService extends IService<SysPersonnel> {

    /**
     * 查询所有施工人员
     *
     * @return 施工人员列表
     */
    public List<SysPersonnel> selectPersonnelAll();

    public SysPersonnel login(String personnelPhone, String personnelPassword);

    public List<SysPersonnel> selectPersonnelList(SysPersonnel personnel);

    public int insertPersonnel(SysPersonnel personnel);

    /**
     * 校验施工人员手机号
     *
     * @param personnel 施工人员信息
     * @return 结果
     */
    public String checkPersonnelPhoneUnique(SysPersonnel personnel);

    /**
     * 通过施工人员ID查询信息
     *
     * @param personnelId 施工人员ID
     * @return 角色对象信息
     */
    public SysPersonnel selectPersonnelById(Long personnelId);

    /**
     * 通过施工人员ID查询施工人员使用数量
     *
     * @param personnelId 施工人员ID
     * @return 结果
     */
    public int countUserPersonnelById(Long personnelId);

    /**
     * 批量删除岗位信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     * @throws Exception 异常
     */
    public int deletePersonnelByIds(String ids) throws Exception;

    /**
     * 修改施工人员信息
     *
     * @param personnel 施工人员信息
     * @return 结果
     */
    public int updatePersonnel(SysPersonnel personnel);

    public SysPersonnel selectPersonnelByIds(Long personnelId);

    public SysPersonnel getUser(Long personnelId);

    public SysPersonnel selectPersonnelConstructionStatus(Long personnelId);

    public SysPersonnel selectConstructionIdNumber(Long personnelId,String constructionLine,String shopNumber);

    public int getPassWord(SysPersonnel personnel);
}
