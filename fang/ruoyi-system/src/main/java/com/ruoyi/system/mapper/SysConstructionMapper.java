package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysConstruction;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface SysConstructionMapper {
    /**
     * 查询系统所有施工（含按钮）
     *
     * @return 施工列表
     */
    public List<SysConstruction> selectConstructionList(SysConstruction sysConstruction);

    /**
     * 校验门店行号
     *
     * @param shopNumber 施工信息
     * @return 结果
     */
    public SysConstruction checkShopNumberUnique(String shopNumber);

    /**
     * 新增施工信息
     *
     * @param construction 施工信息
     * @return 结果
     */
    public int insertShopNumber(SysConstruction construction);


    /**
     * 通过施工ID查询信息
     *
     * @param constructionId 施工ID
     * @return 角色对象信息
     */
    public SysConstruction selectConstructionById(Long constructionId);

    /**
     * 修改施工人员信息
     *
     * @param construction 施工人员信息
     * @return 结果
     */
    public int updateConstruction(SysConstruction construction);

    /**
     * 查看当前施工信息
     *
     * @param constructionId 查看当前施工信息
     * @return 结果
     */
    public SysConstruction getConstruction(Long constructionId);

    /**
     * 更改当前施工状态
     *
     * @param construction 更改当前施工信息
     * @return 结果
     */
    public int getConstructionStatus(SysConstruction construction);

    public SysConstruction getConstructionStatuss(Long constructionId);

    /**
     * 查看当前门店施工记录
     *
     * @param personnelId,shopId 查看当前门店施工记录
     * @return 结果
     */
    public List<SysConstruction> getShopRecord(@Param("personnelId") Long personnelId ,@Param("shopId") Long shopId);

    public int upload(SysConstruction construction);

    public SysConstruction getConstructionReport(Long constructionId);
}
