package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysConstructionImg;


import java.util.List;

public interface SysConstructionImgMapper {
    public int batchConstructionImg(List<SysConstructionImg> constructionImgList);
}
