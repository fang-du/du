package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysPersonnel;
import com.ruoyi.common.core.domain.entity.SysShop;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysShopMapper;
import com.ruoyi.system.mapper.SysUserShopOneMapper;
import com.ruoyi.system.service.ISysShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysShopServiceImpl implements ISysShopService {

    @Autowired
    private SysShopMapper shopMapper;

    @Autowired
    private SysUserShopOneMapper shopOneMapper;

    @Override
    public List<SysShop> selectShopList(SysShop shop) {
        return shopMapper.selectShopList(shop);
    }

    @Override
    public int insertShop(SysShop shop) {
        return shopMapper.insertShop(shop);
    }

    @Override
    public String checkShopPhoneUnique(SysShop shop) {
        Long shopId = StringUtils.isNull(shop.getShopId()) ? -1L : shop.getShopId();
        SysShop info = shopMapper.checkShopPhoneUnique(shop.getPhone());
        if (StringUtils.isNotNull(info) && info.getShopId().longValue() != shopId.longValue())
        {
            return UserConstants.SHOP_PHONE_ONE_NOT_UNIQUE;
        }
        return UserConstants.SHOP_PHONE_ONE_UNIQUE;
    }

    @Override
    public int updateShop(SysShop shop) {
        return shopMapper.updateShop(shop);
    }

    @Override
    public SysShop selectShopById(Long shopId) {
        return shopMapper.selectShopById(shopId);
    }

    @Override
    public int countUserOneShopById(Long personnelId) {
        return shopOneMapper.countUserOneShopById(personnelId);
    }

    /**
     * 批量删除岗位信息
     *
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    public int deleteShopByIds(String ids) throws Exception {
        Long[] shIds = Convert.toLongArray(ids);
        for (Long shId :shIds)
        {
            SysShop shop = selectShopById(shId);
            if (countUserOneShopById(shId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", shop.getPhone()));
            }
        }
        return shopMapper.deleteShopByIds(shIds);
    }

    @Override
    public SysShop selectShopConstruction(Long constructionId) {
        return shopMapper.selectShopConstruction(constructionId);
    }
}
