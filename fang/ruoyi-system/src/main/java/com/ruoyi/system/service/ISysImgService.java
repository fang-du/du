package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysImg;

public interface ISysImgService {
    public int inserConstructionId(Long constructionId);

    public int insertImg(SysImg img);
}
