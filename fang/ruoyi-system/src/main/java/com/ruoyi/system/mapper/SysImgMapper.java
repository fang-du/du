package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysImg;

public interface SysImgMapper {
    public int inserConstructionId(Long constructionId);

    public int insertImg(SysImg img);
}
