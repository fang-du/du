package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysConstruction;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysConstrPerson;
import com.ruoyi.system.domain.SysConstructionImg;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysConstrPersonMapper;
import com.ruoyi.system.mapper.SysConstructionImgMapper;
import com.ruoyi.system.mapper.SysConstructionMapper;
import com.ruoyi.system.service.ISysConstructionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysConstructionServiceImpl implements ISysConstructionService {
    @Autowired
    private SysConstructionMapper sysConstructionMapper;

    @Autowired
    private SysConstrPersonMapper constrPersonMapper;


    @Override
    public List<SysConstruction> selectConstructionList(SysConstruction sysConstruction) {
        return sysConstructionMapper.selectConstructionList(sysConstruction);
    }

    @Override
    public String checkShopNumberUnique(SysConstruction construction) {
        Long constructionId = StringUtils.isNull(construction.getConstructionId()) ? -1L : construction.getConstructionId();
        SysConstruction info = sysConstructionMapper.checkShopNumberUnique(construction.getShopNumber());
        if (StringUtils.isNotNull(info) && info.getConstructionId().longValue() != constructionId.longValue()) {
            return UserConstants.SHOP_NUMBER_NOT_UNIQUE;
        }
        return UserConstants.SHOP_NUMBER_UNIQUE;
    }


    @Override
    public int insertShopNumber(SysConstruction construction) {

       /* // 新增用户信息
        int rows = userMapper.insertUser(user);
        // 新增用户岗位关联
        insertUserPost(user);
        // 新增用户与角色管理
        insertUserRole(user.getUserId(), user.getRoleIds());
        return rows;*/
        int rows = sysConstructionMapper.insertShopNumber(construction);
        insertConstrPerson(construction.getConstructionId(),construction.getPersonnelIds());

        return rows;
    }

    @Override
    public SysConstruction selectConstructionById(Long constructionId) {
        return sysConstructionMapper.selectConstructionById(constructionId);
    }

    @Override
    public int updateConstruction(SysConstruction construction) {
        return sysConstructionMapper.updateConstruction(construction);
    }

    @Override
    public SysConstruction getConstruction(Long constructionId) {
        return sysConstructionMapper.getConstruction(constructionId);
    }

    @Override
    public int getConstructionStatus(SysConstruction construction) {
        return sysConstructionMapper.getConstructionStatus(construction);
    }

    @Override
    public SysConstruction getConstructionStatuss(Long constructionId) {
        return sysConstructionMapper.getConstructionStatuss(constructionId);
    }

    @Override
    public List<SysConstruction> getShopRecord(Long personnelId, Long shopId) {
        return sysConstructionMapper.getShopRecord(personnelId, shopId);
    }

    @Override
    public int upload(SysConstruction construction) {
        return sysConstructionMapper.upload(construction);
    }

    @Override
    public SysConstruction getConstructionReport(Long constructionId) {
        return sysConstructionMapper.getConstructionReport(constructionId);
    }

    public void insertConstrPerson(Long constructionId ,Long[] personnelIds){
        if (StringUtils.isNotNull(personnelIds)) {
            List<SysConstrPerson> list = new ArrayList<SysConstrPerson>();
            for (Long personnelId : personnelIds)
            {
                SysConstrPerson si = new SysConstrPerson();
                si.setConstructionId(constructionId);
                si.setPersonnelId(personnelId);
                list.add(si);
            }
            if (list.size() > 0)
            {
                constrPersonMapper.batchConstrPerson(list);
            }
        }
    }
}