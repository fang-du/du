package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysConstruction;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ISysConstructionService {
    /**
     * 查询系统所有施工（含按钮）
     *
     * @return 施工列表
     */
    public List<SysConstruction> selectConstructionList(SysConstruction sysConstruction);

    /**
     * 校验门店行号
     *
     * @param construction 施工信息
     * @return 结果
     */
    public String checkShopNumberUnique(SysConstruction construction);

    /**
     * 新增施工信息
     *
     * @param construction 施工信息
     * @return 结果
     */
    public int insertShopNumber(SysConstruction construction);

    /**
     * 通过施工ID查询信息
     *
     * @param constructionId 施工ID
     * @return 角色对象信息
     */
    public SysConstruction selectConstructionById(Long constructionId);

    /**
     * 修改施工人员信息
     *
     * @param construction 施工人员信息
     * @return 结果
     */
    public int updateConstruction(SysConstruction construction);

    public SysConstruction getConstruction(Long constructionId);

    public int getConstructionStatus(SysConstruction construction);

    public SysConstruction getConstructionStatuss(Long constructionId);

    public List<SysConstruction> getShopRecord (Long personnelId , Long shopId);

    public int upload(SysConstruction construction);

    public SysConstruction getConstructionReport(Long constructionId);
}
