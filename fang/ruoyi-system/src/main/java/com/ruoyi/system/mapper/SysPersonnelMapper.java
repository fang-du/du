package com.ruoyi.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.core.domain.entity.SysPersonnel;
import org.apache.ibatis.annotations.Param;
import java.util.List;

public interface SysPersonnelMapper extends BaseMapper<SysPersonnel> {
    public List<SysPersonnel> selectPersonnelList(SysPersonnel personnel);

     SysPersonnel login(@Param("personnelPhone") String personnelPhone , @Param("personnelPassword") String personnelPassword);

    public int insertPersonnel(SysPersonnel personnel);

    /**
     * 校验施工人员手机号
     *
     * @param personnelPhone 施工人员信息
     * @return 结果
     */
    public SysPersonnel checkPersonnelPhoneUnique(String personnelPhone);

    /**
     * 批量删除岗位信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePersonnelByIds(Long[] ids);


    /**
     * 通过施工人员ID查询信息
     *
     * @param personnelId 施工人员ID
     * @return 角色对象信息
     */
    public SysPersonnel selectPersonnelById(Long personnelId);

    /**
     * 修改施工人员信息
     *
     * @param personnel 施工人员信息
     * @return 结果
     */
    public int updatePersonnel(SysPersonnel personnel);

    public SysPersonnel selectPersonnelConstructionByIds(Long personnelId);

    public SysPersonnel selectPersonnelConstructionStatus(Long personnelId);

    public SysPersonnel getUser(Long personnelId);

    public SysPersonnel selectConstructionIdNumber(@Param("personnelId") Long personnelId,@Param("constructionLine") String constructionLine,@Param("shopNumber") String shopNumber);

    public int getPassWord(SysPersonnel personnel);
}
