package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysPersonnel;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.mapper.SysPersonnelMapper;
import com.ruoyi.system.mapper.SysUserShopMapper;
import com.ruoyi.system.service.ISysPersonnelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysPersonnelServiceImpl extends ServiceImpl<SysPersonnelMapper, SysPersonnel> implements ISysPersonnelService {
    @Autowired
    private SysPersonnelMapper personnelMapper;

    @Autowired
    private SysUserShopMapper userShopMapper;

    @Override
    public List<SysPersonnel> selectPersonnelAll() {
        return SpringUtils.getAopProxy(this).selectPersonnelList(new SysPersonnel());
    }

    @Override
    public SysPersonnel login(String personnelPhone, String personnelPassword) {
        return personnelMapper.login(personnelPhone,personnelPassword);
    }

    @Override
    public List<SysPersonnel> selectPersonnelList(SysPersonnel personnel) {
        return personnelMapper.selectPersonnelList(personnel);
    }

    @Override
    public int insertPersonnel(SysPersonnel personnel) {
        return personnelMapper.insertPersonnel(personnel);
    }

    @Override
    public String checkPersonnelPhoneUnique(SysPersonnel personnel) {
        Long shopId = StringUtils.isNull(personnel.getPersonnelId()) ? -1L : personnel.getPersonnelId();
        SysPersonnel info = personnelMapper.checkPersonnelPhoneUnique(personnel.getPersonnelPhone());
        if (StringUtils.isNotNull(info) && info.getPersonnelId().longValue() != shopId.longValue())
        {
            return UserConstants.SHOP_PHONE_NOT_UNIQUE;
        }
        return UserConstants.SHOP_PHONE_UNIQUE;
    }

    @Override
    public SysPersonnel selectPersonnelById(Long personnelId) {
        return personnelMapper.selectPersonnelById(personnelId);
    }

    @Override
    public int countUserPersonnelById(Long personnelId) {
        return userShopMapper.countUserShopById(personnelId);
    }


   /* @Override
    public int deletePostByIds(String ids)
    {
        Long[] postIds = Convert.toLongArray(ids);
        for (Long postId : postIds)
        {
            SysPost post = selectPostById(postId);
            if (countUserPostById(postId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", post.getPostName()));
            }
        }
        return postMapper.deletePostByIds(postIds);
    }*/

    /**
     * 批量删除岗位信息
     *
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    public int deletePersonnelByIds(String ids) throws Exception {
        Long[] personnelIds = Convert.toLongArray(ids);
        for (Long personnelId :personnelIds)
        {
            SysPersonnel personnel = selectPersonnelById(personnelId);
            if (countUserPersonnelById(personnelId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", personnel.getPersonnelPhone()));
            }
        }
        return personnelMapper.deletePersonnelByIds(personnelIds);
    }

    @Override
    public int updatePersonnel(SysPersonnel personnel) {
        return personnelMapper.updatePersonnel(personnel);
    }

    @Override
    public SysPersonnel selectPersonnelByIds(Long personnelId) {
        return personnelMapper.selectPersonnelConstructionByIds(personnelId);
    }

    @Override
    public SysPersonnel getUser(Long personnelId) {
        return personnelMapper.getUser(personnelId);
    }

    @Override
    public SysPersonnel selectPersonnelConstructionStatus(Long personnelId) {
        return personnelMapper.selectPersonnelConstructionStatus(personnelId);
    }

    @Override
    public SysPersonnel selectConstructionIdNumber(Long personnelId, String constructionLine, String shopNumber) {
        return personnelMapper.selectConstructionIdNumber(personnelId,constructionLine,shopNumber);
    }

    @Override
    public int getPassWord(SysPersonnel personnel) {
        return personnelMapper.getPassWord(personnel);
    }


}
