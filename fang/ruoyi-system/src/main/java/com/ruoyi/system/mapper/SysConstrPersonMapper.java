package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysConstrPerson;


import java.util.List;

public interface SysConstrPersonMapper {
    public int batchConstrPerson(List<SysConstrPerson> constrPersonList);
}
