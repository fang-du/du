package com.ruoyi.system.mapper;




import com.ruoyi.common.core.domain.entity.SysShop;
import java.util.List;

public interface SysShopMapper {

    public List<SysShop> selectShopList(SysShop shop);

    public int insertShop(SysShop shop);

    /**
     * 校验门店手机号
     *
     * @param shopPhone 施工人员信息
     * @return 结果
     */
    public SysShop checkShopPhoneUnique(String shopPhone);

    /**
     * 修改门店信息
     *
     * @param shop 门店信息
     * @return 结果
     */
    public int updateShop(SysShop shop);

    /**
     * 通过门店ID查询信息
     *
     * @param shopId 门店ID
     * @return 门店对象信息
     */
    public SysShop selectShopById(Long shopId);

    /**
     * 批量删除门店信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteShopByIds(Long[] ids);

    public SysShop selectShopConstruction(Long constructionId);
}
