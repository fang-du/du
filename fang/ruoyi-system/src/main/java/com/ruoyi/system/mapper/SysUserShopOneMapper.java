package com.ruoyi.system.mapper;

public interface SysUserShopOneMapper {

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param shopId 岗位ID
     * @return 结果
     */
    public int countUserOneShopById(Long shopId);
}
