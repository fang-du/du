package com.ruoyi.system.mapper;

public interface SysUserShopMapper {

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param shopId 岗位ID
     * @return 结果
     */
    public int countUserShopById(Long shopId);
}
