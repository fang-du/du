package com.ruoyi.common.core.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.Date;
import java.util.List;

public class SysConstruction extends BaseEntity {

    @Excel(name = "施工序号", cellType = Excel.ColumnType.NUMERIC)
    /** 施工id */
    private Long constructionId;

    @Excel(name = "施工单号")
    /** 施工单号 */
    private String constructionNumber;

    @Excel(name = "施工类型")
    /** 施工类型 */
    private String constructionType;

    @Excel(name = "门店行号")
    /** 门店行号 */
    private String shopNumber;

    @Excel(name = "施工城市")
    /** 施工城市 */
    private String city;

    @Excel(name = "施工门店")
    /** 施工门店 */
    private String constructionShop;

    @Excel(name = "施工人员")
    /** 施工人员 */
    private String constructionPersonnel;

    @Excel(name = "施工状态")
    /** 施工状态:0未验收,1已验收 */
    private String statuss;

    @Excel(name = "施工完成时间")
    /** 施工完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date  finishTime;

    @Excel(name = "施工图片")
    /** 施工图片 */
    private String constructionImg;

    @Excel(name = "施工行号")
    /** 施工行号 */
    private String constructionLine;

    @Excel(name = "经度")
    /** 经度 */
    private String longitude;

    @Excel(name = "纬度")
    /** 纬度 */
    private String latitude;

    private Long personnelId;

    private List<SysPersonnel> personnels;

    private SysShop shop;

    private List<SysImg> imgList;

    /** 施工人员组 */
    private Long[] personnelIds;

    @Excel(name = "施工描述")
    /** 施工描述 */
    private String constructionDescribe;

    public Long getConstructionId() {
        return constructionId;
    }

    public void setConstructionId(Long constructionId) {
        this.constructionId = constructionId;
    }

    public String getConstructionNumber() {
        return constructionNumber;
    }

    public void setConstructionNumber(String constructionNumber) {
        this.constructionNumber = constructionNumber;
    }

    public String getConstructionType() {
        return constructionType;
    }

    public void setConstructionType(String constructionType) {
        this.constructionType = constructionType;
    }

    public String getShopNumber() {
        return shopNumber;
    }

    public void setShopNumber(String shopNumber) {
        this.shopNumber = shopNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getConstructionShop() {
        return constructionShop;
    }

    public void setConstructionShop(String constructionShop) {
        this.constructionShop = constructionShop;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getConstructionPersonnel() {
        return constructionPersonnel;
    }

    public void setConstructionPersonnel(String constructionPersonnel) {
        this.constructionPersonnel = constructionPersonnel;
    }

    public String getConstructionImg() {
        return constructionImg;
    }

    public void setConstructionImg(String constructionImg) {
        this.constructionImg = constructionImg;
    }

    public String getConstructionDescribe() {
        return constructionDescribe;
    }

    public void setConstructionDescribe(String constructionDescribe) {
        this.constructionDescribe = constructionDescribe;
    }

    public String getConstructionLine() {
        return constructionLine;
    }

    public void setConstructionLine(String constructionLine) {
        this.constructionLine = constructionLine;
    }

    public String getStatuss() {
        return statuss;
    }

    public void setStatuss(String statuss) {
        this.statuss = statuss;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Long getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(Long personnelId) {
        this.personnelId = personnelId;
    }

    public List<SysPersonnel> getPersonnels() {
        return personnels;
    }

    public void setPersonnels(List<SysPersonnel> personnels) {
        this.personnels = personnels;
    }

    public SysShop getShop() {
        return shop;
    }

    public void setShop(SysShop shop) {
        this.shop = shop;
    }

    public List<SysImg> getImgList() {
        return imgList;
    }

    public void setImgList(List<SysImg> imgList) {
        this.imgList = imgList;
    }

    public Long[] getPersonnelIds() {
        return personnelIds;
    }

    public void setPersonnelIds(Long[] personnelIds) {
        this.personnelIds = personnelIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("constructionId",getConstructionId())
                .append("constructionNumber",getConstructionNumber())
                .append("constructionType",getConstructionType())
                .append("shopNumber",getShopNumber())
                .append("createBy",getCreateBy())
                .append("city",getCity())
                .append("constructionShop",getConstructionShop())
                .append("constructionPersonnel",getConstructionPersonnel())
                .append("createTime",getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("statuss",getStatuss())
                .append("finishTime",getFinishTime())
                .append("constructionImg",getConstructionImg())
                .append("constructionDescribe",getConstructionDescribe())
                .append("constructionLine",getConstructionLine())
                .append("longitude",getLongitude())
                .append("latitude",getLatitude())
                .append("personnelId",getPersonnelId())
                .toString();
    }
}
