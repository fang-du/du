package com.ruoyi.common.core.domain.entity;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.List;

public class SysShop extends BaseEntity {
    /** 门店id */
    @Excel(name = "门店序号", cellType = Excel.ColumnType.NUMERIC)
    private Long shopId;

    /** 行号 */
    @Excel(name = "行号")
    private String lineNumber;

    /** 门店名 */
    @Excel(name = "门店名")
    private String shopName;

    /** 所属城市 */
    @Excel(name = "所属城市")
    private String city;

    /** 地址 */
    @Excel(name = "地址")
    private String site;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contact;

    /** 手机 */
    @Excel(name = "手机")
    private String phone;

    /** 座机 */
    @Excel(name = "座机")
    private String specialPhone;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    private List<SysConstruction> constructionList;

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpecialPhone() {
        return specialPhone;
    }

    public void setSpecialPhone(String specialPhone) {
        this.specialPhone = specialPhone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SysConstruction> getConstructionList() {
        return constructionList;
    }

    public void setConstructionList(List<SysConstruction> constructionList) {
        this.constructionList = constructionList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("shopId",getShopId())
                .append("lineNumber",getLineNumber())
                .append("shopName",getShopName())
                .append("city",getCity())
                .append("site", getSite())
                .append("contact", getContact())
                .append("phone",getPhone())
                .append("specialPhone",getSpecialPhone())
                .append("status",getStatus())
                .append("updateTime",getUpdateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .toString();
    }
}
