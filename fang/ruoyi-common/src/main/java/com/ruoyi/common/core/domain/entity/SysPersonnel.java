package com.ruoyi.common.core.domain.entity;



import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.io.Serializable;
import java.util.List;

public class SysPersonnel extends BaseEntity implements Serializable {

    /** 施工人员id */
    @Excel(name = "施工人员序号", cellType = Excel.ColumnType.NUMERIC)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long personnelId;

    @Excel(name = "手机号")
    /** 施工人员手机号 */
    private String personnelPhone;

    @Excel(name = "密码")
    /** 施工人员密码 */
    private String personnelPassword;

    @Excel(name = "姓名")
    /** 施工人员姓名 */
    private String personnelName;

    @Excel(name = "工号")
    /** 施工人员工号 */
    private String workNumber;

    @Excel(name = "身份证号")
    /** 施工人员身份证号 */
    private String idNumber;

    @Excel(name = "工作证照片")
    /** 施工人员工作证照片 */
    private String cardImg;

    @Excel(name = "身份证照片")
    /** 施工人员身份证照片 */
    private String idImg;

    @Excel(name = "入职时间")
    /** 入职时间 */
    private String entryTime;

    private List<SysConstruction> constructionList;

    /** 人员状态（0正常 1停用） */
    @Excel(name = "人员状态", readConverterExp = "0=正常,1=停用")
    private String status;

    public boolean isAdmin()
    {
        return isAdmin(this.personnelId);
    }

    public static boolean isAdmin(Long personnelId)
    {
        return personnelId != null && 1L == personnelId;
    }

    public Long getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(Long personnelId) {
        this.personnelId = personnelId;
    }

    public String getPersonnelPhone() {
        return personnelPhone;
    }

    public void setPersonnelPhone(String personnelPhone) {
        this.personnelPhone = personnelPhone;
    }

    public String getPersonnelPassword() {
        return personnelPassword;
    }

    public void setPersonnelPassword(String personnelPassword) {
        this.personnelPassword = personnelPassword;
    }

    public String getPersonnelName() {
        return personnelName;
    }

    public void setPersonnelName(String personnelName) {
        this.personnelName = personnelName;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getCardImg() {
        return cardImg;
    }

    public void setCardImg(String cardImg) {
        this.cardImg = cardImg;
    }

    public String getIdImg() {
        return idImg;
    }

    public void setIdImg(String idImg) {
        this.idImg = idImg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SysConstruction> getConstructionList() {
        return constructionList;
    }

    public void setConstructionList(List<SysConstruction> constructionList) {
        this.constructionList = constructionList;
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("personnelId",getPersonnelId())
                .append("personnelPhone",getPersonnelPhone())
                .append("personnelPassword",getPersonnelPassword())
                .append("createTime",getCreateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("personnelName",getPersonnelName())
                .append("workNumber",getWorkNumber())
                .append("idNumber",getIdNumber())
                .append("cardImg",getCardImg())
                .append("idImg",getIdImg())
                .append("status",getStatus())
                .append("entryTime",getEntryTime())
                .toString();
    }
}
