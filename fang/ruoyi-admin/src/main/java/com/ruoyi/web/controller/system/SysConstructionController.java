package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.config.ServerConfig;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysConstruction;
import com.ruoyi.common.core.domain.entity.SysImg;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.ISysConstructionService;
import com.ruoyi.system.service.ISysImgService;
import com.ruoyi.system.service.ISysPersonnelService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/system/construction")
public class SysConstructionController extends BaseController
{
    private String prefix = "system/construction";

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private ISysConstructionService constructionService;

    @Autowired
    private ISysPersonnelService personnelService;

    @Autowired
    private ISysImgService imgService;

    @RequiresPermissions("system:construction:view")
    @GetMapping()
    public String construction()
    {
        return prefix + "/construction";
    }

    /*@PostMapping("/expor")
    @ResponseBody
    public AjaxResult expor(Long constructionId){
        List<SysConstruction> list = constructionService.getConstructionReport(constructionId);
        ExcelUtil<SysConstruction> util = new ExcelUtil<SysConstruction>(SysConstruction.class);
        return util.exportExcel(list, "施工报告数据");
    }*/

    /**
     * 施工列表
     */
   // @RequiresPermissions("system:construction:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysConstruction sysConstruction)
    {
        startPage();
        List<SysConstruction> list = constructionService.selectConstructionList(sysConstruction);
        return getDataTable(list);
    }

    @Log(title = "施工管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:construction:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysConstruction construction)
    {
        List<SysConstruction> list = constructionService.selectConstructionList(construction);
        ExcelUtil<SysConstruction> util = new ExcelUtil<SysConstruction>(SysConstruction.class);
        return util.exportExcel(list, "施工数据");
    }

    /**
     * 新增施工
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("personnels", personnelService.selectPersonnelAll().stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        return prefix + "/add";
    }

    /**
     * 新增保存施工
     */
    @RequiresPermissions("system:construction:add")
    @Log(title = "施工管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysConstruction construction)
    {

        if (UserConstants.SHOP_NUMBER_NOT_UNIQUE.equals(constructionService.checkShopNumberUnique(construction)))
        {
            return error("新增施工'" + construction.getShopNumber() + "'失败，门店行号已存在");
        }
        construction.setCreateBy(getLoginName());
        System.out.println(construction);
        return toAjax(constructionService.insertShopNumber(construction));
    }

    /**
     * 校验施工行号
     */
    @PostMapping("/checkShopNumberUnique")
    @ResponseBody
    public String checkShopNumberUnique(SysConstruction construction)
    {
        return constructionService.checkShopNumberUnique(construction);
    }

    /**
     * 修改施工信息
     */
    @GetMapping("/edit/{constructionId}")
    public String edit(@PathVariable("constructionId") Long constructionId, ModelMap mmap)
    {
        mmap.put("construction", constructionService.selectConstructionById(constructionId));
        return prefix + "/edit";
    }

    /**
     * 修改施工信息
     */
    @RequiresPermissions("system:construction:edit")
    @Log(title = "施工管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysConstruction construction)
    {
        construction.setUpdateBy(getLoginName());
        return toAjax(constructionService.updateConstruction(construction));
    }

    /**
     * 查看施工详情
     */
    @GetMapping("/getConstruction")
    @ResponseBody
    public SysConstruction getConstruction(Long constructionId){
        return constructionService.getConstruction(constructionId);
    }

    /**
     * 修改施工状态
     */
    @GetMapping("/getConstructionStatus")
    @ResponseBody
    public int getConstructionStatus (@Validated Long constructionId){
        SysConstruction construction = constructionService.getConstructionStatuss(constructionId);
        String sta = construction.getStatuss();
        if (sta.equals("1")){
            construction.setStatuss("2");
        }

        return constructionService.getConstructionStatus(construction);
    }


    @GetMapping("/getShopRecord")
    @ResponseBody
    public List<SysConstruction> getShopRecord(Long personnelId,Long shopId){
        return constructionService.getShopRecord(personnelId,shopId);
    }

    @GetMapping("/upload")
    @ResponseBody
    public void upload(Long constructionId){
        SysConstruction construction = constructionService.getConstruction(constructionId);
        construction.setConstructionImg("1");
        System.out.println(construction);
    }

    @RequestMapping("/ss")
    public String ss(){
        return prefix+"/ss";
    }


    /**
     * 上传施工图片
     */
    @PostMapping("/uploads")
    @ResponseBody
    public AjaxResult uploadFile(MultipartFile file,Long constructionId) throws Exception
    {
        try
        {
            SysConstruction construction = constructionService.getConstruction(constructionId);
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            String url = serverConfig.getUrl() + fileName;
            construction.setConstructionImg(filePath);
            constructionService.upload(construction);
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }
    }

    @PostMapping("/upload2QiniuForMd")
    @ResponseBody
    public Object upload2QiniuForMd(@RequestParam("file") MultipartFile[] file,Long constructionId) {
        //这个是图片类型,校验图片是什么类型如jpg
        List<String> filePath = uploadToQiniu(file, null, false,constructionId);
        Map<String, Object> resultMap = new HashMap<>(3);
        resultMap.put("success", 1);
        if (filePath != null) {
            resultMap.put("message", "上传成功");
        } else {
            resultMap.put("message", "上传失败");
        }
        resultMap.put("filename", filePath);
        return resultMap;
    }

    private List<String> uploadToQiniu(MultipartFile[] file, Object uploadType, boolean canBeNull,Long constructionId) {
        if (!canBeNull && null == file) {
            return null;
        }
        // 可为空并且file为空，忽略后边的代码，返回null
        if (canBeNull && null == file) {
            return null;
        }
        //文件保存位置
        String filePath = "D:\\ruoyi\\uploadPath\\upload";
        System.out.println(filePath);
        FileOutputStream out = null;
        String fileName = null;
        List<String> imgurl = new ArrayList<>();
        for (MultipartFile mfil : file) {
            if (!mfil.isEmpty()) {
                try {
                    fileName = FileUploadUtils.upload(filePath, mfil);
                    imgurl.add(filePath + fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        SysImg sysImg = new SysImg();
        imgurl.forEach(v -> {
            sysImg.setImgName(v);
            sysImg.setConstructionId(constructionId);
            imgService.insertImg(sysImg);
        });
        return imgurl;
    }

    @GetMapping("/getConstructionReport")
    @ResponseBody
    public SysConstruction getConstructionReport(Long constructionId){
        return constructionService.getConstructionReport(constructionId);
    }
}
