package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysPersonnel;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.ISysPersonnelService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/personnel")
@CrossOrigin(origins = "*")
public class SysPersonnelController extends BaseController {
    private String prefix = "system/personnel";

    @Autowired
    private ISysPersonnelService personnelService;

    @RequiresPermissions("system:personnel:view")
    @GetMapping()
    public String construction()
    {
        return prefix + "/personnel";
    }

    /**
     * 施工工人员列表
     */
//    @RequiresPermissions("system:personnel:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysPersonnel personnel)
    {
        startPage();
        List<SysPersonnel> list = personnelService.selectPersonnelList(personnel);
        return getDataTable(list);
    }

    @Log(title = "施工人员管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:personnel:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysPersonnel personnel)
    {
        List<SysPersonnel> list = personnelService.selectPersonnelList(personnel);
        ExcelUtil<SysPersonnel> util = new ExcelUtil<SysPersonnel>(SysPersonnel.class);
        return util.exportExcel(list, "施工人员数据");
    }



    /**
     * 新增施工人员
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }



    /**
     * 新增保存施工人员
     */
    @RequiresPermissions("system:personnel:add")
    @Log(title = "施工人员管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysPersonnel personnel)
    {

        if (UserConstants.SHOP_PHONE_NOT_UNIQUE.equals(personnelService.checkPersonnelPhoneUnique(personnel)))
        {
            return error("新增施工人员'" + personnel.getPersonnelPhone() + "'失败，施工人员手机号已存在");
        }
        personnel.setCreateBy(getLoginName());
        String res = "";
        if (!StringUtils.isEmpty(personnel.getIdNumber())) {
            StringBuilder stringBuilder = new StringBuilder(personnel.getIdNumber());
            res = stringBuilder.replace(6, 14, "********").toString();
        }
        personnel.setIdNumber(res);
        System.out.println(personnel);

        return toAjax(personnelService.insertPersonnel(personnel));
    }

    /**
     * 校验施工人员手机号
     */
    @PostMapping("/checkPersonnelPhoneUnique")
    @ResponseBody
    public String checkShopNumberUnique(SysPersonnel shop)
    {
        return personnelService.checkPersonnelPhoneUnique(shop);
    }

    @RequiresPermissions("system:personnel:remove")
    @Log(title = "施工人员管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        try
        {
            return toAjax(personnelService.deletePersonnelByIds(ids));
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
    }


    /**
     * 修改施工人员信息
     */
    @GetMapping("/edit/{personnelId}")
    public String edit(@PathVariable("personnelId") Long personnelId, ModelMap mmap)
    {
        mmap.put("personnel", personnelService.selectPersonnelById(personnelId));
        return prefix + "/edit";
    }

    /**
     * 修改施工人员信息
     */
    @RequiresPermissions("system:personnel:edit")
    @Log(title = "施工人员管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysPersonnel personnel)
    {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(personnelService.checkPersonnelPhoneUnique(personnel)))
        {
            return error("修改岗位'" + personnel.getPersonnelPhone() + "'失败，施工人员号码已存在");
        }
        personnel.setUpdateBy(getLoginName());
        return toAjax(personnelService.updatePersonnel(personnel));
    }

    @RequestMapping("/")
    public String index(){
        return prefix+"/login";
    }

    @Autowired
    private HttpServletRequest req;
    /**
     * 前台登录
     */
    @PostMapping("/login")
    @ResponseBody
    public Map login(@RequestBody SysPersonnel personnel)
    {
        SysPersonnel login = personnelService.login(personnel.getPersonnelPhone(),personnel.getPersonnelPassword());
        req.getServletContext().setAttribute("login",login);
        if(login != null)
        {
            String s = login.toString();
            Map map = new HashMap();
            map.put("code",0);
            map.put("msg","登录成功");
            map.put("personnelId",login.getPersonnelId());
            return map;
        }
        else
        {
            String msg = "用户或密码错误";
            Map map = new HashMap();
            map.put("msg",msg);
            return map;
        }
    }

    /**
     * 查看当前登录信息
     */
    @GetMapping ("/getUser{personnelId}")
    @ResponseBody
    public SysPersonnel getUser(Long personnelId){
        return personnelService.getUser(personnelId);
    }

    /**
     * 查看个人施工记录
     */
    @GetMapping("/getConstruction")
    @ResponseBody
    public SysPersonnel getConstruction(Long personnelId){
        return personnelService.selectPersonnelByIds(personnelId);
    }

    @GetMapping("/getConstructionStatus")
    @ResponseBody
    public SysPersonnel getConstructionStatus(Long personnelId){
        return personnelService.selectPersonnelConstructionStatus(personnelId);
    }

    @GetMapping("/getConstructionIdNumber")
    @ResponseBody
    public SysPersonnel getConstructionIdNumber(Long personnelId,String constructionLine,String shopNumber){
        return personnelService.selectConstructionIdNumber(personnelId,constructionLine,shopNumber);
    }

    @GetMapping("/getPassWord")
    @ResponseBody
    public Object getPassWord(Long personnelId, String personnelPassword, String newPassword){
        SysPersonnel personnel = personnelService.getUser(personnelId);
        if(!personnel.getPersonnelPassword().equals(personnelPassword)){
            Map map = new HashMap();
            map.put("error","密码输入错误");
            return map;
        }
        else {
            personnel.setPersonnelPassword(newPassword);
            personnelService.getPassWord(personnel);
            Map map = new HashMap();
            map.put("success","修改密码成功");
            return map;
        }
    }
}
