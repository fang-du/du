package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysShop;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.ISysShopService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/system/shop")
@CrossOrigin
public class SysShopController extends BaseController {
    private String prefix = "system/shop";

    @Autowired
    private ISysShopService shopService;

    @RequiresPermissions("system:shop:view")
    @GetMapping()
    public String construction()
    {
        return prefix + "/shop";
    }

    /**
     * 门店列表
     */
    @RequiresPermissions("system:shop:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysShop shop)
    {
        startPage();
        List<SysShop> list = shopService.selectShopList(shop);
        return getDataTable(list);
    }

    @Log(title = "门店管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:shop:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysShop shop)
    {
        List<SysShop> list = shopService.selectShopList(shop);
        ExcelUtil<SysShop> util = new ExcelUtil<SysShop>(SysShop.class);
        return util.exportExcel(list, "门店数据");
    }

    /**
     * 新增门店
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 校验施工人员手机号
     */
    @PostMapping("/checkShopPhoneUnique")
    @ResponseBody
    public String checkShopNumberUnique(SysShop shop)
    {
        return shopService.checkShopPhoneUnique(shop);
    }

    /**
     * 新增保存门店
     */
    @RequiresPermissions("system:shop:add")
    @Log(title = "门店管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated SysShop shop)
    {
        if (UserConstants.SHOP_PHONE_ONE_NOT_UNIQUE.equals(shopService.checkShopPhoneUnique(shop)))
        {
            return error("新增门店'" + shop.getPhone() + "'失败，门店手机号已存在");
        }
        shop.setCreateBy(getLoginName());
        return toAjax(shopService.insertShop(shop));
    }

    /**
     * 修改门店信息
     */
    @GetMapping("/edit/{shopId}")
    public String edit(@PathVariable("shopId") Long shopId, ModelMap mmap)
    {
        mmap.put("shop", shopService.selectShopById(shopId));
        return prefix + "/edit";
    }

    /**
     * 修改门店信息
     */
    @RequiresPermissions("system:shop:edit")
    @Log(title = "门店管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated SysShop shop)
    {
        if (UserConstants.POST_NAME_NOT_UNIQUE.equals(shopService.checkShopPhoneUnique(shop)))
        {
            return error("修改门店'" + shop.getPhone() + "'失败，门店号码已存在");
        }
        shop.setUpdateBy(getLoginName());
        return toAjax(shopService.updateShop(shop));
    }

    /**
     * 删除门店信息
     */
    @RequiresPermissions("system:shop:remove")
    @Log(title = "门店管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        try
        {
            return toAjax(shopService.deleteShopByIds(ids));
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
    }

    @GetMapping("/getShopConstruction")
    @ResponseBody
    public SysShop getShopConstruction(Long constructionId){
        return shopService.selectShopConstruction(constructionId);
    }
}
