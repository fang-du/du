import Vue from 'vue'
import App from './App'

import uView from 'uview-ui'
Vue.use(uView);

Vue.config.productionTip = false
import tools from './common/js/tools.js'
import Ajax from './common/js/ajax.js'
import { myRequest } from './util/util.js'
Vue.prototype.$myRequest = myRequest

// import request from './util/util.js'
// Vue.prototype.request = request
Vue.prototype.$tools=tools
Vue.prototype.$Ajax=Ajax
App.mpType = 'app'

// 引m '入全局uView



const app = new Vue({
    ...App
})
app.$mount()
