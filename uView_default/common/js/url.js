// 版本号
//正式
const IP = 'www.jkzg.com'
const IP1 = 'www.jkzg.com/'
var ISHTTPS = false;
var version = "1.0.0";

// 测试
// const IP = '47.241.170.196:18082'
// const IP1 = '47.241.170.196:18082'
// var ISHTTPS = false;
// var version = "5.2.0";

//本地 
// const IP = '192.168.110.180:18383'
// const IP1 = '192.168.110.180:18383'
// var ISHTTPS = false;
// var version = "1.0.0";


const webUrl = (ISHTTPS ? 'https://' : 'http://') + IP;
const basePthURL = (ISHTTPS ? 'https://' : 'http://') + IP1;

export default {
	webUrl: webUrl,
	basePthURL: basePthURL,
	version: version
}
