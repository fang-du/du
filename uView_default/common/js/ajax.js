import webUrl from './url.js'
import $tools from './tools.js'

const ajax = (url, data, successCallback, failCallBack, type) => {
	var token = uni.getStorageSync('token');
	data.token = token;
	// if(noToken){
	// 	data.token = ''
	// }
	
	// manifest.json 配置地址

	uni.request({
		url: webUrl.webUrl + url,
		data: data,
		method: type || 'POST',
		dataType: 'json',
		header: {
			 "x-requested-with": "XMLHttpRequest",
			 // "content-type": "application/x-www-form-urlencoded",
			 "content-type": "application/json",
			 "token": token,
			 "lang":'zh/en'
		},
		success: (res) => {
			// console.log(webUrl.webUrl);
			var code = res.data.code;
			if (code == '00000') {
				successCallback(res.data);
			} else if (code == '00017') {
				if (getApp().globalData.is_flag) { 
					getApp().globalData.is_flag = false;
					console.log('没有登录');
					setTimeout(function() {
						$tools.jump('../firstPage/firstPage');
						// $tools.jump('../auth/login');
						uni.removeStorageSync('token');
					}, 100)
					setTimeout(() => {
						getApp().globalData.is_flag = true;
						console.log(getApp().globalData.is_flag);
					}, 1000)
				}
			}  else {
				$tools.toast(res.data.msg);
				successCallback(res.data);
			} 
		},
		fail: () => {
			// this.$tools.toast('连接失败,请稍后再试');
			// $tools.toast('请稍后再试');
		}
	})
}
export default ajax
